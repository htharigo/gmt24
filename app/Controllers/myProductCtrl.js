'use strict';
/** 
 * controllers used for the login
 */
app.controller('myProductCtrl', function ($rootScope, $scope, $http, $location,$timeout,$window, $state, userService) {

if(!localStorage.getItem("userInfo"))
{
   $state.go('frontend.home', {reload:true})
}
    
$scope.data = {};
$scope.user = {};
//alert('a');

if($window.localStorage["currency"] != ''){
   $scope.usersSelectedCurrency = $window.localStorage["currency"] ;
}else{
    $scope.usersSelectedCurrency = 'KWD';
}

 $window.scrollTo(0, 0);
if ($window.localStorage["userInfo"]) {
var userInfo = JSON.parse($window.localStorage["userInfo"]);	
	$scope.user_id=userInfo.user_id;
        $scope.getCurrentUserType();
}
else {
	var userInfo={};
	userInfo.user_id ="";
	//$scope.user_id=userInfo.user_id;
}

$scope.isform1 =0;
	$scope.form1 = function(user) {

	$scope.isform1 =1;

}



 
$scope.myproduct = function(){
   // alert('hii');

 userService.myproduct($scope.usersSelectedCurrency).then(function(response) {
     
    
		
		//$scope.isExists=response.Ack;
		if(response.Ack == '1') {
                    $scope.exists=1;
		$scope.productLists=response.productList;
		console.log('spproduct',$scope.productLists);
                //$window.localStorage["userzip"]='';
		
		} else {
                    
                    $scope.productLists='';
                     $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	}); 
	
        }
        

 $scope.deleteProduct = function(id){
          // alert(cat_id);
           // return false;
           
            swal({
            title: "Are you sure, you want to delete this?",
            text: "Once deleted, you will not be able to recover this !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              userService.deleteProduct(id).then(function(response) {
		//console.log(response.Ack);
	
		if(response.Ack == '1') {
                    swal({
                        title:"Your product has been deleted !",
      icon: "success",
      buttons: true,
            dangerMode: true,
    }) .then((result) => {
                    if(result.value == true){
                        $window.location.reload();
                           $scope.myproduct();
                    }
                  //swal(`The returned value is: ${value}`);
                });;
                    $scope.exists=1;
                  //  $scope.user='';
		
               // $scope.user_idd=$scope.user_id;
		//console.log($scope.alljobs);	
		
		} else {
                    console.log('ppp');	
                    $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});
            } else {
              
            }
          });
            /* userService.deleteProduct(id).then(function(response) {
		//console.log(response.Ack);
	
		if(response.Ack == '1') {
                    console.log(response);
                   // alert('Added Successfully.');
                   // $window.location.reload()
                    $scope.exists=1;
                  //  $scope.user='';
		$scope.myproduct();
               // $scope.user_idd=$scope.user_id;
		//console.log($scope.alljobs);	
		
		} else {
                    console.log('ppp');	
                    $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	}); */    
        
       
        
}

$scope.deleteProductpermanently = function(id){
           
           // return false;
           
            swal({
            title: "Are you sure, you want to delete this?",
            text: "Once deleted, you will not be able to recover this !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                  //alert(id);
                
              userService.deleteProductpermanently(id).then(function(response) {
		//console.log('delpro',response.Ack);
              
		if(response.Ack == '1') {
                    swal({
                        title:"Your product has been deleted !",
      icon: "success",
    }) .then((value) => {
                    if(value == true){
                        $window.location.reload()
                           $scope.myproduct();
                    }
                  //swal(`The returned value is: ${value}`);
                });;
                    $scope.exists=1;
                  //  $scope.user='';
		
               // $scope.user_idd=$scope.user_id;
		//console.log($scope.alljobs);	
		
		} else {
                    console.log('ppp');	
                    $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});
            } else {
              
            }
          });
              
        
       
        
}

$scope.editProduct = function(id){
          // alert(cat_id);
           // return false;
             userService.editProduct(id).then(function(response) {
		//console.log(response.Ack);
	
		if(response.Ack == '1') {
                    console.log(response);
                   // alert('Added Successfully.');
                   // $window.location.reload()
                    $scope.exists=1;
                  //  $scope.user='';
		$scope.myproduct();
               // $scope.user_idd=$scope.user_id;
		//console.log($scope.alljobs);	
		
		} else {
                    console.log('ppp');	
                    $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});     
        
       
        
}

 $scope.sendforauction = function(id){
        
           $state.go('frontend.sendForAuction',{product_id:id});

              
}

$scope.pay = function(lid){
        
           
           $state.go('frontend.userpayment',{pid:lid}); 

              
}





$scope.marksold = function(id){
          // alert(cat_id);
           // return false;
           
             swal({
            title: "Are you sure, you want to mark this as sold?",
            text: "Once makred as sold, you will not be able to recover this !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              userService.markProduct(id).then(function(response) {
		//console.log(response.Ack);
	
		if(response.Ack == '1') {
                    swal("Your product has been makred !", {
      icon: "success",
    }) .then((value) => {
                    if(value == true){

                           $scope.myproduct();
                    }
                  //swal(`The returned value is: ${value}`);
                });;
                    $scope.exists=1;
                  //  $scope.user='';
		
               // $scope.user_idd=$scope.user_id;
		//console.log($scope.alljobs);	
		
		} else {
                    console.log('ppp');	
                    $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});
            } else {
              
            }
          });
           
           
            /* userService.markProduct(id).then(function(response) {
		//console.log(response.Ack);
	
		if(response.Ack == '1') {
                    console.log(response);
                   // alert('Added Successfully.');
                   // $window.location.reload()
                    $scope.exists=1;
                  //  $scope.user='';
		$scope.myproduct();
               // $scope.user_idd=$scope.user_id;
		//console.log($scope.alljobs);	
		
		} else {
                    console.log('ppp');	
                    $scope.exists=0;
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});  */   
        
       
        
}


$scope.marktop = function(lid){
        
           
           $state.go('frontend.usertoppayment',{pid:lid}); 

              
}





	
});

