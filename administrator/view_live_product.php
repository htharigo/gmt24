<?php
include_once("./includes/config.php");
include_once('includes/session.php');
?>

<?php
if (isset($_REQUEST['submit'])) {
    $price = isset($_POST['price']) ? $_POST['price'] : '';
    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $movement = isset($_POST['movement']) ? $_POST['movement'] : '';
    $gender = isset($_POST['gender']) ? $_POST['gender'] : '';
    $brand = isset($_POST['brand']) ? $_POST['brand'] : '';
    $reference_number = isset($_POST['reference_number']) ? $_POST['reference_number'] : '';
    $date_purchase = isset($_POST['date_purchase']) ? $_POST['date_purchase'] : '';
    $status_watch = isset($_POST['status_watch']) ? $_POST['status_watch'] : '';
    $owner_number = isset($_POST['owner_number']) ? $_POST['owner_number'] : '';
    $country = isset($_POST['country']) ? $_POST['country'] : '';
    $state = isset($_POST['state']) ? $_POST['state'] : '';
    $date_purchase = isset($_POST['date_purchase']) ? $_POST['date_purchase'] : '';
    $city = isset($_POST['city']) ? $_POST['city'] : '';
    $size = isset($_POST['size']) ? $_POST['size'] : '';
    $model_year = isset($_POST['model_year']) ? $_POST['model_year'] : '';
    $currency_code= isset($_POST['currency_code']) ? $_POST['currency_code'] : '';
    $location = isset($_POST['location']) ? $_POST['location'] : '';
    $description = isset($_POST['description']) ? $_POST['description'] : '';


    $fields = array(
        'price' => mysqli_real_escape_string($con, $price),
        'name' => mysqli_real_escape_string($con, $name),
        'movement' => mysqli_real_escape_string($con, $movement),
        'gender' => mysqli_real_escape_string($con, $gender),
        'brands' => mysqli_real_escape_string($con, $brand),
        'owner_number' => mysqli_real_escape_string($con, $owner_number),
        'reference_number' => mysqli_real_escape_string($con, $reference_number),
        'date_purchase' => mysqli_real_escape_string($con, $date_purchase),
        'status_watch' => mysqli_real_escape_string($con, $status_watch),
        'country' => mysqli_real_escape_string($con, $country),
        'size' => mysqli_real_escape_string($con, $size),
        'state' => mysqli_real_escape_string($con, $state),
        'date_purchase' => mysqli_real_escape_string($con, $date_purchase),
        'model_year' => mysqli_real_escape_string($con, $model_year),
        'currency_code' => mysqli_real_escape_string($con, $currency_code),
        'location' => mysqli_real_escape_string($con, $location),
        'description' => mysqli_real_escape_string($con, $description),
        'city' => mysqli_real_escape_string($con, $city),
    );

    $fieldsList = array();
    foreach ($fields as $field => $value) {
        $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
    }

    if ($_REQUEST['action'] == 'edit' && $_REQUEST['page'] == 'Request') {

        $getuploaderid = mysqli_fetch_array(mysqli_query($con, "SELECT * from webshop_products where id='" . mysqli_real_escape_string($con, $_REQUEST['id']) . "'"));
        $isFeePaid = $getuploaderid['auction_fee_paid'];


        $editQuery = "UPDATE `webshop_products` SET " . implode(', ', $fieldsList)
                . " WHERE `id` = '" . mysqli_real_escape_string($con, $_REQUEST['id']) . "'";


        mysqli_query($con, $editQuery);


        header('Location:list_product.php');
        exit();
    }
    
    else if ($_REQUEST['action'] == 'edit' && $_REQUEST['page'] == 'live') {

        $getuploaderid = mysqli_fetch_array(mysqli_query($con, "SELECT * from webshop_products where id='" . mysqli_real_escape_string($con, $_REQUEST['id']) . "'"));
        $isFeePaid = $getuploaderid['auction_fee_paid'];


        $editQuery = "UPDATE `webshop_products` SET " . implode(', ', $fieldsList)
                . " WHERE `id` = '" . mysqli_real_escape_string($con, $_REQUEST['id']) . "'";


        mysqli_query($con, $editQuery);


        header('Location:list_live_product.php');
        exit();
    }
}


if ($_REQUEST['action'] == 'edit') {
    $categoryRowset = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM `webshop_products` WHERE `id`='" . mysqli_real_escape_string($con, $_REQUEST['id']) . "'"));
}


$datefordatepicker = array();
$fetch_subscription = mysqli_query($con, "select * from webshop_auctiondates");
$num = mysqli_num_rows($fetch_subscription);
if ($num > 0) {

    $i = 0;
    while ($subscription = mysqli_fetch_array($fetch_subscription)) {
        $date = explode('-', stripslashes($subscription['date']));
        $datefordatepicker[$i] = $date[2] . '-' . $date[1] . '-' . $date[0];
        $i++;
    }
}
//$result = $conn->query("SELECT * FROM webshop_auctiondates");
//echo 'hi';
//echo '<pre>';
//print_r($datefordatepicker);
//exit;
?>

<!-- Header Start -->
<?php include ("includes/header.php"); ?>
<!-- Header End -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Product <small><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?>  Product</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <span class="divider">/</span>
                        </li>
                        <li>
                            <a href="#"> Product</a>
                            <span class="divider">/</span>
                        </li>

                        <li>
                            <span><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Product</span>

                        </li>





                    </ul>
                    
                   <button onclick="goBack()">Go Back</button>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Edit Product</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            
                            <div class="control-group" id="image_delete">
                                    
                                    <div class="controls">
                                        <?php
                                        $sqlimage = "SELECT * FROM webshop_product_image WHERE product_id='".$categoryRowset['id']."'";
                                        $result = mysqli_query($con, $sqlimage);
                                        ?>
                                       
                                            <?php
                                            
                                            while ($row = mysqli_fetch_array($result)) {
                                                ?>
                                    <div class="row-fluid">
                                        <div class="span2">
                                        <div class="row-fluid product-img">
                                            <div class="span12">
                                                <label class="control-label">Images</label>
                                            </div>
                                            <div class="span12 text-center">
                                                <img src=".././upload/product_image/<?php echo $row['image']; ?>" size="">
                                            </div>
                                            <div class="span12 del-bt">
                                                <?php //echo $categoryRowset['id'];?><?php //echo $row['id'];?>
                                                <input type="radio" name="marks"  onclick="makeprimaryimages(<?php echo $row['id'];?>,<?php echo $categoryRowset['id'];?>)" <?php if($categoryRowset['image'] == $row['image']){echo 'checked';}?> name="primaryimage" value="<?php echo $row['id'];?>_<?php echo $categoryRowset['id'];?>"> Mark as primary image<br>
                                                <a href="delete_image.php?product_id=<?php echo $categoryRowset['id'];?>&id=<?php echo $row['id'];?>&type=<?php echo 1;?>&page=live" class="btn"> Delete</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                                 
                                            <?php }
                                            ?>

                                    
                                    </div>
                                </div>
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                                
                                
<!--                                <div class="control-group">
                                    <label class="control-label">Name</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter name" value="<?php echo $categoryRowset['name']; ?>" name="name" readonly>
                                    </div>
                                </div>-->

                                <div class="control-group">
                                    <label class="control-label">Price</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter price" value="<?php echo $categoryRowset['price']; ?>" name="price"  >
                                    </div>
                                </div>

                                <!--<div class="control-group">
                               <label class="control-label">Description</label>
                               <div class="controls">
                               <textarea rows="4" cols="50" class="ckeditor form-control" placeholder="Enter text" name ="description"><?php echo $categoryRowset['description']; ?></textarea>
                               </div>
                               </div>-->




                                <div class="control-group">
                                    <label class="control-label">Movement</label>
                                    <div class="controls">
                                        <?php
                                        $sql = "SELECT * FROM webshop_movement";
                                        $result = mysqli_query($con, $sql);
                                        ?>
                                        <select name='movement'>
                                            <option value=''> Select movement</option>
                                            <?php
                                            while ($rowmovement = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value='<?php echo $rowmovement['name']; ?>'  <?php if ($rowmovement['name'] == $categoryRowset['movement']) { ?> selected="selected"<?php } ?>><?php echo $rowmovement['name']; ?></option>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>
<!--                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter movement" value="<?php echo $categoryRowset['movement']; ?>" name="movement" readonly>
                                    </div>-->
                                </div>


                                <div class="control-group">
                                    <label class="control-label">Gender</label>
                                    <div class="controls">
                                        <input type="radio" name="gender" value="Female" <?php if ('Female' == $categoryRowset['gender']) { ?> checked <?php } ?> >Female&nbsp;
                                        <input type="radio" name="gender" value="Male" <?php if ('Male' == $categoryRowset['gender']) { ?> checked <?php } ?> >Male<br>
                                        <input type="radio" name="gender" value="Unisex" <?php if ('Unisex' == $categoryRowset['gender']) { ?> checked <?php } ?> >Unisex<br>
                                    </div>
                                </div>

                                <!--   <div class="control-group">
                                     <label class="control-label">Brand</label>
                                     <div class="controls">
                                     <input type="text" class="form-control" placeholder="Enter brand" value="<?php echo $getBrands['name']; ?>" name="brand" readonly>
                                     </div>
                                 </div> -->
                                
                                <div class="control-group">
                                    <label class="control-label">Brand</label>
                                    <div class="controls">
                                        <?php
                                        $sql = "SELECT * FROM webshop_brands";
                                        $result = mysqli_query($con, $sql);
                                        ?>
                                        <select name='brand' onchange="getCategory(this.value)">
                                            <option value=''> Select Brand</option>
                                            <?php
                                            while ($row = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value='<?php echo $row['id']; ?>'  <?php if ($row['id'] == $categoryRowset['brands']) { ?> selected="selected"<?php } ?>><?php echo $row['name']; ?></option>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Category</label>
                                    <div class="controls">
                                        <?php
                                        $sql = "SELECT * FROM webshop_category";
                                        $result = mysqli_query($con, $sql);
                                        ?>
                                        <select name="category_id" id="category_id">
                                            <option value=''> Select Category</option>
                                            <?php
                                            while ($row = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value='<?php echo $row['id']; ?>'  <?php if ($row['id'] == $categoryRowset['cat_id']) { ?> selected="selected"<?php } ?>><?php echo $row['name']; ?></option>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                
                                
                                <div class="control-group">
                                    <label class="control-label">Bracelet</label>
                                    <div class="controls">
                                        <?php
                                        $sql = "SELECT * FROM webshop_bracelet";
                                        $result = mysqli_query($con, $sql);
                                        ?>
                                        <select name='breslet_type'>
                                            <option value=''> Select Bracelet</option>
                                            <?php
                                            while ($row = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value='<?php echo $row['id']; ?>'  <?php if ($row['id'] == $categoryRowset['breslet_type']) { ?> selected="selected"<?php } ?>><?php echo $row['type']; ?></option>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                
                                <div class="control-group">
                                    <label class="control-label">Reference Number</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter reference number" value="<?php echo $categoryRowset['reference_number']; ?>" name="reference_number">
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Owner Number</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter owner number" value="<?php echo $categoryRowset['owner_number']; ?>" name="owner_number" >
                                    </div>
                                </div>

                             <div class="control-group">
                                    <label class="control-label">Date of Purchase</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter purchase date" value="<?php echo $categoryRowset['date_purchase']; ?>" id="datepickerpurchase" name="date_purchase" >
                                    </div>
                                </div>
                                <?php
                                  $last_year = date("Y");

                                    for ($year = $last_year; $year >= 1900; $year--) {

                                        array_push($allYears, $year);
                                    }
                                    ?>
                                <div class="control-group">
                                    <label class="control-label">Model Year</label>
                                    <div class="controls">
                                        <select name='model_year'>
                                          <?php
                                    $last_year = date("Y");

                                    for ($year = $last_year; $year >= 1900; $year--) {
                                         ?>
                                        <option value='<?php echo $year; ?>'  <?php if ($year == $categoryRowset['model_year']) { ?> selected="selected"<?php } ?>><?php echo $year; ?></option>
                                   <?php   }
                                    ?>
                                        </select>
                                        <!--<input type="text" class="form-control" placeholder="Enter Model Year" value="<?php echo $categoryRowset['model_year']; ?>" name="model_year" >-->
                                    </div>
                                </div>

                                 <div class="control-group">
                                     <label class="control-label">Currency Code</label>
                                     <div class="controls">
                                         <input type="text" class="form-control" placeholder="Enter currency code" value="<?php echo $categoryRowset['currency_code']; ?>" name="currency_code" >
                                     </div>
                                 </div>
                                
                                
                                
                                <div class="control-group">
                                    <label class="control-label">Status of watch</label>
                                    <div class="controls">
                                        <?php
                                

                                $sql = "SELECT * FROM webshop_watchstatus";
                                $result = mysqli_query($con,$sql);
                                ?>
                                        <select name='status_watch'>
                                  <option value=''> Select status</option>
                                <?php 
                                while ($row = mysqli_fetch_array($result)) {
                                                                    ?>
                            <option value='<?php echo $row['status'];?>'  <?php if($row['status']== $categoryRowset['status_watch']){?> selected="selected"<?php }?>><?php echo $row['status'];?></option>
                                    <?php 
                                } ?>

                             </select>
                                    </div>
                                </div>
                                
                                
                                
                                
                                <div class="control-group">
                                     <label class="control-label">Work hours</label>
                                     <div class="controls">
                                         <input type="text" class="form-control" placeholder="Enter work hours" value="<?php echo $categoryRowset['work_hours']; ?>"  name="work_hours" >
                                     </div>
                                 </div>
                                
                                
                                
                                
                                <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls">
                                <textarea rows="4" cols="50" class="ckeditor form-control" placeholder="Enter text" name ="description" ><?php echo $categoryRowset['description'];?></textarea>
                                </div> 
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Country</label>
                                    <div class="controls">
                                        <?php
                                        $sql = "SELECT * FROM webshop_countries";
                                        $resultcountry = mysqli_query($con, $sql);
                                        ?>
                                        <select name='country' id="country" onchange="getState(this.value)">
                                            <option value=''> Select Country</option>
                                            <?php
                                            while ($row = mysqli_fetch_array($resultcountry)) {
                                                ?>
                                                <option value='<?php echo $row['id']; ?>'  <?php if ($row['id'] == $categoryRowset['country']) { ?> selected="selected"<?php } ?>><?php echo $row['name']; ?></option>
                                            <?php }
                                            ?>

                                        </select>

                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">State</label>
                                    <div class="controls">
                                        <?php
                                        $sql = "SELECT * FROM `webshop_states`";
                                        $resultstates = mysqli_query($con, $sql);
                                        ?>
                                        <select name='state' id="state" >
                                            <option value=''> Select State</option>
                                            <?php
                                            while ($row = mysqli_fetch_array($resultstates)) {
                                                ?>
                                                <option value='<?php echo $row['id']; ?>'  <?php if ($row['id'] == $categoryRowset['state']) { ?> selected="selected"<?php } ?>><?php echo $row['name']; ?></option>
                                            <?php }
                                            ?>

                                        </select>

                                    </div>
                                </div>

<!--                                <div class="control-group">
                                    <label class="control-label">City</label>
                                    <div class="controls" >
                                        <?php
                                        $sql = "SELECT * FROM 	webshop_cities";
                                        $resultcities = mysqli_query($con, $sql);
                                        ?>
                                        <div id="city">
                                        <select name='city' id="city" disabled>
                                            <option value=''> Select State</option>
                                            <?php
                                            while ($row = mysqli_fetch_array($resultcities)) {
                                                ?>
                                                <option value='<?php echo $row['id']; ?>'  <?php if ($row['id'] == $categoryRowset['city']) { ?> selected="selected"<?php } ?>><?php echo $row['name']; ?></option>
                                            <?php }
                                            ?>

                                        </select>
                                        </div>

                                    </div>
                                </div>-->
                                <div class="control-group">
                                    <label class="control-label">Model Year</label>
                                    <div class="controls">
                                        
                                        <select name='model_year'>
                                  <option value=''> Select Model Year</option>
                                <?php 
                                $curryear = date('Y');
                                for ($year = 1900;$year<=$curryear;$year++) {
                                                                    ?>
                            <option value='<?php echo $year;?>'  <?php if($year == $categoryRowset['model_year']){?> selected="selected"<?php }?>><?php echo $year ;?></option>
                                    <?php 
                                } ?>

                             </select>
                                        <!--<input type="text" class="form-control" placeholder="Enter Model Year" value="<?php echo $categoryRowset['model_year']; ?>" name="model_year" required>-->
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Size</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Enter size" value="<?php echo $categoryRowset['size']; ?>" name="size">
                                    </div>
                                </div>


                                    <div class="form-group map-search">
                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">

                           <div id="dvMap" style="width: 500px; height: 500px"></div>
                        </div>
                        <div class="form-group">
                           <label for="staticEmail">Location</label>
                           <!--<p class="" id="addressshow">Choose_your_location </p>-->
<!--                           <input type="hidden" class="form-control" id="address" value="" placeholder="" ng-pattern="/^[a-z A-Z]+$/" ng-model="user2.location"
                              name="location" readonly>-->
                        </div>

                                <!--    <div class="control-group">
                                       <label class="control-label">Location</label>
                                       <div class="controls">
                                           <input type="text" class="form-control" placeholder="Enter location" value="<?php echo $categoryRowset['location']; ?>" name="location" readonly>
                                       </div>
                                   </div>
   
                                   <div class="control-group">
                                       <label class="control-label">Preferred Date</label>
                                       <div class="controls">
                                           <input type="text" class="form-control" placeholder="Enter preferred_date" id="datepicker" value="<?php echo $categoryRowset['preferred_date']; ?>" name="preferred_date" readonly>
                                       </div>
                                   </div>
   
   
                                <!--   <div class="control-group">
                                      <label class="control-label">Start Date</label>
                                      <div class="controls">
                                      <input type="text" class="form-control" placeholder="Enter start date" id="datepicker" value="<?php echo $categoryRowset['start_date_time']; ?>" name="start_date_time" readonly>
                                      </div>
                                  </div>
  
                                  <div class="control-group">
                                      <label class="control-label">End Date</label>
                                      <div class="controls">
                                      <input type="text" class="form-control" placeholder="Enter end date" id="datepickerr" value="<?php echo $categoryRowset['end_date_time']; ?>" name="end_date_time" readonly>
                                      </div>
                                  </div> 

                                <div class="control-group">
                                    <label class="control-label">Image Upload</label>
                                    <div class="controls">
                                        <input type="file" name="imagee" class=" btn blue"  ><?php if ($categoryRowset['image'] != '') { ?><br><a href="../upload/product_image/<?php echo $categoryRowset['image']; ?>" target="_blank">View</a><?php } ?>
                                    </div>
                                </div>
                                -->
                                
                                 <div class="control-group">
                                     <label class="control-label">Location</label>
                                     <div class="controls">
                                         <input type="text" id="address" class="form-control" placeholder="Enter location" value="<?php echo $categoryRowset['location']; ?>" name="location" readonly>
                                     </div>
                                 </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                                    <button type="reset" id="cancle_product_edit" class="btn"><i class=" icon-remove"></i> Cancel</button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">

                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- Footer Start -->

<?php include("includes/footer.php"); ?>

<!-- Footer End -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="js/jquery-1.8.3.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script> 
<!--<script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->
<script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>

<!-- ie8 fixes -->
<!--[if lt IE 9]>
<script src="js/excanvas.js"></script>
<script src="js/respond.js"></script>
<![endif]-->

<script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
<script src="js/jquery.sparkline.js" type="text/javascript"></script>
<script src="assets/chart-master/Chart.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.datepicker.js"></script>
<link href="css/jquery.datepicker.css" rel="stylesheet">
<script src="assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<!--common script for all pages-->
<script src="js/common-scripts.js"></script>

<!--script for this page only-->

<script src="js/easy-pie-chart.js"></script>
<script src="js/sparkline-chart.js"></script>
<script src="js/home-page-calender.js"></script>
<script src="js/home-chartjs.js"></script>

<script>

    jQuery(function () {

        var enableDays = <?php echo json_encode($datefordatepicker); ?>;

        function enableAllTheseDays(date) {
            var sdate = $.datepicker.formatDate('dd-mm-yy', date)
            if ($.inArray(sdate, enableDays) != -1) {
                return [true, 'event'];
            }
            return [false];
        }

        $('#datepicker').datepicker({dateFormat: 'dd-mm-yy', beforeShowDay: enableAllTheseDays});
    })</script>
<script>
    $(document).ready(function () {
        $("#datepickerr").datepicker({dateFormat: 'yy-mm-dd'});
    });</script>
<script>
    $(document).ready(function () {
        $("#datepickerpurchase").datepicker({dateFormat: 'yy-mm-dd'});
    });</script>
<script>
    function getSubCategory(val) {
        $.ajax({
            type: "POST",
            url: "get_subcategory.php",
            data: 'category_id=' + val,
            success: function (data) {
                $("#subcategory_list").html(data);
            }
        });
    }
    
     function getState(val) {
        $.ajax({
            type: "POST",
            url: "get_state.php",
            data: 'country=' + val,
            success: function (data) {
                $("#state").html(data);
            }
        });
    }
    
    function getCategory(val) {
        $.ajax({
            type: "POST",
            url: "get_brand.php",
            data: 'brand_id=' + val,
            success: function (data) {
                $("#category_id").html(data);
            }
        });
    }
    $("#cancle_product_edit").click(function(){
    window.location.href = "list_product.php";
}); 

function goBack() {
    window.history.back()
};
</script>
<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
<style>
    .event a {
        background-color: #D3D3D3 !important;
        background-image :none !important;
        color: #008000 !important;
    }
    .ui-datepicker .ui-state-default {
        color: #696969 ;
        background: ghostwhite;
    }
</style>



<style>
   /* Absolute Center Spinner */
   .loading {
   position: fixed;
   z-index: 999;
   height: 2em;
   width: 2em;
   overflow: show;
   margin: auto;
   top: 0;
   left: 0;
   bottom: 0;
   right: 0;
   }
   /* Transparent Overlay */
   .loading:before {
   content: '';
   display: block;
   position: fixed;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
   background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
   }
   /* :not(:required) hides these rules from IE9 and below */
   .loading:not(:required) {
   /* hide "loading..." text */
   font: 0/0 a;
   color: transparent;
   text-shadow: none;
   background-color: transparent;
   border: 0;
   }
   .loading:not(:required):after {
   content: '';
   display: block;
   font-size: 10px;
   width: 1em;
   height: 1em;
   margin-top: -0.5em;
   -webkit-animation: spinner 1500ms infinite linear;
   -moz-animation: spinner 1500ms infinite linear;
   -ms-animation: spinner 1500ms infinite linear;
   -o-animation: spinner 1500ms infinite linear;
   animation: spinner 1500ms infinite linear;
   border-radius: 0.5em;
   -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
   box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
   }
   /* Animation */
   @-webkit-keyframes spinner {
   0% {
   -webkit-transform: rotate(0deg);
   -moz-transform: rotate(0deg);
   -ms-transform: rotate(0deg);
   -o-transform: rotate(0deg);
   transform: rotate(0deg);
   }
   100% {
   -webkit-transform: rotate(360deg);
   -moz-transform: rotate(360deg);
   -ms-transform: rotate(360deg);
   -o-transform: rotate(360deg);
   transform: rotate(360deg);
   }
   }
   @-moz-keyframes spinner {
   0% {
   -webkit-transform: rotate(0deg);
   -moz-transform: rotate(0deg);
   -ms-transform: rotate(0deg);
   -o-transform: rotate(0deg);
   transform: rotate(0deg);
   }
   100% {
   -webkit-transform: rotate(360deg);
   -moz-transform: rotate(360deg);
   -ms-transform: rotate(360deg);
   -o-transform: rotate(360deg);
   transform: rotate(360deg);
   }
   }
   @-o-keyframes spinner {
   0% {
   -webkit-transform: rotate(0deg);
   -moz-transform: rotate(0deg);
   -ms-transform: rotate(0deg);
   -o-transform: rotate(0deg);
   transform: rotate(0deg);
   }
   100% {
   -webkit-transform: rotate(360deg);
   -moz-transform: rotate(360deg);
   -ms-transform: rotate(360deg);
   -o-transform: rotate(360deg);
   transform: rotate(360deg);
   }
   }
   @keyframes spinner {
   0% {
   -webkit-transform: rotate(0deg);
   -moz-transform: rotate(0deg);
   -ms-transform: rotate(0deg);
   -o-transform: rotate(0deg);
   transform: rotate(0deg);
   }
   100% {
   -webkit-transform: rotate(360deg);
   -moz-transform: rotate(360deg);
   -ms-transform: rotate(360deg);
   -o-transform: rotate(360deg);
   transform: rotate(360deg);
   }
   }
   .fa {
   position: absolute;
   left: 18%;
   top: 3%;
   }
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDnTqCZqlvuWG1KNrNh9slnA_EqiJmiX9g&callback=initMap&libraries=places"
   async defer></script>
   
   <script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">


      function initMap() {
     if (navigator.geolocation) {
       navigator.geolocation.getCurrentPosition(showPosition);
     } else {
       x.innerHTML = "Geolocation is not supported by this browser.";
     }
   }




      function showPosition(position) {
//        var map = new google.maps.Map(document.getElementById('dvMap'), {
//          center: {lat: -33.8688, lng: 151.2195},
//          zoom: 13,
//          mapTypeId: 'roadmap'
//        });
        
        var myLatLng = { lat: position.coords.latitude, lng: position.coords.longitude };
   
     $('#lat').val(position.coords.latitude);
     $('#long').val(position.coords.longitude);
   console.log('spandan',position);
   
     var map = new google.maps.Map(document.getElementById('dvMap'), {
       zoom: 13,
       center: myLatLng,
       mapTypeId: 'roadmap'
     });
        // Create the search box and link it to the UI element.
        
        var input = document.getElementById('pac-input');
        //alert(input);
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            
                 //==========================
          if(place.geometry.location){       
                //alert('1') ;
          var marker = new google.maps.Marker({
                position: place.geometry.location,
                map: map,
                title: place.name,
                draggable: true,
              });
   
   
   
             /*$('#lat').val(lat);
             $('#long').val(lng);
             $('#address').val(address);
             */
            $('#address').val(place.formatted_address);
             //$("#addressshow").html(place.formatted_address);
            google.maps.event.addListener(marker, 'dragend', function () {
              geocodePosition(marker.getPosition());
            });
            //================
        }else{
            //alert('2');
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: place.name,
                draggable: true,
              });
   
   
            google.maps.event.addListener(marker, 'dragend', function () {
              geocodePosition(marker.getPosition());
            });
            
        } 
            
             function geocodePosition(pos) {
       var lat, lng, address;
       geocoder = new google.maps.Geocoder();
       geocoder.geocode
         ({
           latLng: pos
         },
         function (results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             lat = pos.lat();
             lng = pos.lng();
             address = results[0].formatted_address;
             //alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);
             $('#lat').val(lat);
             $('#long').val(lng);
             $('#address').val(address);
             $("#addressshow").html(address);
           }
           else {
             $("#mapErrorMsg").html('Cannot determine address at this location.' + status).show(100);
           }
         }
         );
     }
            
            
            


            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>


   
<script>
  
   $("ul.status-watch").on("click", ".init", function () {
     $(this).closest("ul.status-watch").children('li:not(.init)').toggle();
   });
   
   var allOptions = $("ul.status-watch").children('li:not(.init)');
   $("ul.status-watch").on("click", "li:not(.init)", function () {
     allOptions.removeClass('selected');
     $(this).addClass('selected');
     $("ul.status-watch").children('.init').html($(this).html());
     allOptions.toggle();
   });
 $(document).ready(function() {

    $(".select dt").click(function(e) {
        e.stopPropagation();
        var select = $(this).closest('.select');
        // close all other selects
        $('.select').not(select).find('ul').hide();
      
        select.find('ul').toggle();
        select.find("dt, dd ul").css('border-color', 'rgb(128,128,128)')

        select.find("dt, span, dd ul").css('background-color', 'rgb(255,255,196)')


    });

    $(".select dd ul li a").click(function(e) {
        var text = $(this).html();
        var select = $(this).closest('.select');

        if ((select.data('val') == 'multiple') && (e.ctrlKey)) {
            e.stopPropagation()
            $(this).addClass('selected');
            select.find('dt span').html("(" + select.find('a.selected').length + ")");

        }
        else {
            var text = $(this).html();
            select.find("dd a").removeClass('selected');
            $(this).addClass('selected');
            select.find("dt span").html(text);
            //select.find("dt a").css("background-color", "");
            select.find("dd ul").hide();
        }
    });

    $(document).bind('click', function() {
        $(".select dd ul").hide();
        $(".select dt, .select dd ul").css('border-color', '');
    });

});


       
function makeprimaryimages(imageid,productid){
  location.href = "makeprimaryimage.php?imageid=" + imageid + "&productid=" + productid + "&type=1&page=live"; 
}
    
    //
    
    /*$.ajax({
          type: "POST", 
          url: "makeprimaryimage.php",
          data: { 
              imageid: imageid, 
              productid: productid },
          dataType: "json",
          success: function(response) { alert(response); },
          error: function(xhr, ajaxOptions, thrownError) { alert(xhr.responseText); }
        });
      */

</script>
<style>
    .select {
  margin: 0;
  padding: 0;
}
.select dd, .select dt, .select ul {
  margin: 0px;
  padding: 0px;
}
.select dd {
  position: relative;
}
.select a, .select a:visited {
  color: #000;
  text-decoration: none;
  outline: none;
}
.select dt:hover, .select dd ul:hover {
  border-color: rgb(128,128,128);
}
.select dd ul li a:hover {
  background-color: rgb(112, 146, 190);
  color: #FFF;
}
.select dt {
  background: url(arrow.png) no-repeat scroll right center;
  display: block;
  padding-right: 20px;
  border: 1px solid rgb(170, 170, 170);
  width: 180px;
  overflow: hidden;
}
.select dt span {
  cursor: pointer;
  display: block;
  padding: 4px;
  height: 15px;
}
.select dd ul {
  background: #fff none repeat scroll 0 0;
  border-bottom: 1px solid rgb(170, 170, 170);
  border-left: 1px solid rgb(170, 170, 170);
  border-right: 1px solid rgb(170, 170, 170);
  border-top: 0;
  display: none;
  left: 0px;
  padding: 5px 0px;
  position: absolute;
  top: -1px;
  width: auto;
  min-width: 200px;
  list-style: none;
}
.select dd ul li a {
  padding-left: 10px;
  padding-top: 3px;
  padding-bottom: 3px;
  display: block;
}
.selected {
  background: rgb(195, 195, 195);
}
.header-list, .header-list:hover {
  padding-left: 3px;
  font-weight: bold;
  font-style: italic;
  cursor: pointer;
}
    
</style>