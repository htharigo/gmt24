<?php
include_once("./includes/config.php");
include_once('includes/session.php');
?>

<?php

if (isset($_REQUEST['submit'])) {

    $date = date('Y-m-d');
    $movement = isset($_POST['movement']) ? $_POST['movement'] : '';
    
    $id = isset($_POST['id']) ? $_POST['id'] : '';

    $id1 = '';
    for ($w = 0; $w < count($movement); $w++) {
        $id1 = '';
        $movement1 = $movement[$w];
       
        if (isset($id[$w]) && !empty($id[$w])) {
            $id1 = $id[$w];
        } else {
            $id1 = '';
        }
        if ($id1 != '') {

            $editQuery = "UPDATE `webshop_movement` SET `name`='" . $movement1 . "',`date`='" . $date . "' WHERE `id` = '" . $id1 . "'";
            // exit;
        } else {
            $status = 1;
            $editQuery = "INSERT INTO `webshop_movement` (`name`,`date`,`status`) VALUES ('" . $movement1 . "','" . $date . "','" . $status . "')";
        }

        if (mysqli_query($con, $editQuery)) {

            echo 'hi';
            $_SESSION['msg'] = "Category Updated Successfully";
        } else {
            echo mysql_error();
            $_SESSION['msg'] = "Error occuried while updating Category";
        }
    }
    header('Location:list_movement.php');
    exit();
}
if ($_REQUEST['action'] == 'edit') {
    "SELECT * FROM `webshop_movement` WHERE `id`='" . $_REQUEST['id'];
    $categoryRowset = mysqli_query($con, "SELECT * FROM `webshop_movement` WHERE `id`='" . $_REQUEST['id'] . "'");
    $i = 0;
    while ($array = mysqli_fetch_array($categoryRowset)) {

        $array2[$i]['date'] = $array['date'];
        $array2[$i]['name'] = $array['name'];
       
        $array2[$i]['id'] = $array['id'];
        $i++;
    }
    
}
?>

<!-- Header Start -->
<?php include ("includes/header.php"); ?>
<!-- Header End -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Movement <small><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Movement</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <span class="divider">/</span>
                        </li>
                        <li>
                            <a href="#">Movements</a>
                            <span class="divider">/</span>
                        </li>

                        <li>
                            <span><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Movements</span>

                        </li>





                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Edit Movement</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post">
                                <div id="tbod">

                                    

                                    <div class="input_fields_wrap">
                                        <?php
                                        foreach ($array2 as $v) {
                                            ?>

                                            <div class="control-group" style="width:40%;float:left;">                                                
                                                <label class="control-label">Movement </label>
                                                <div class="controls">
                                                    <input type="text" class="form-control timepicker"id="time_1" placeholder="Movement" value="<?php echo $v['name']; ?>" name="movement[]" required>

                                        
                                                    <input type="hidden" id="" class="form-control" value="<?php echo $v['id']; ?>" name="id[]" required> 
                                                </div>
                                            </div>
                                            
                                            
                                            <div class='clearfix'>&nbsp; </div>


                                            
                                            <?php
                                        }
                                        ?>
                                        <!--<button class="add_field_button">Add More Fields</button>-->
                                    </div>

                                </div>




                        </div>
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                        <button type="reset" class="btn"><i class=" icon-remove"></i> Cancel</button>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">

        </div>
    </div>

    <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- Footer Start -->

<?php include("includes/footer.php"); ?>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/mdtimepicker.css">
<!-- Footer End -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="js/jquery-1.8.3.min.js"></script>
<!--<script src="js/jquery.nicescroll.js" type="text/javascript"></script>-->
<script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<script src="assets/bootstrap/js/bootstrap.min.js"></script>



<script src="js/jquery.sparkline.js" type="text/javascript"></script>

<script src="js/jquery.scrollTo.min.js"></script>


<!--common script for all pages-->
<script src="js/common-scripts.js"></script>




<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
