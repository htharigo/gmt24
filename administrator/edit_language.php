<?php
include_once("./includes/config.php");
include_once('includes/session.php');
?>

<?php

if (isset($_REQUEST['submit'])) {

    $date = date('Y-m-d');
    $transleted_word = isset($_POST['transleted_word']) ? $_POST['transleted_word'] : '';
    //$rating = isset($_POST['rating']) ? $_POST['rating'] : '';
    
    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';

    $editQuery = "UPDATE `webshop_language` SET `transleted_word`='" . $transleted_word . "' WHERE `id` = '" . $id . "'";
             mysqli_query($con,$editQuery);
     
 
     if($_REQUEST['type']=='eng') {
          header('Location:list_english.php');
     }else{
          header('Location:list_arabic.php');
     }
   
   // exit();
}

?>

<!-- Header Start -->
<?php include ("includes/header.php"); ?>
<!-- Header End -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                       Language <small><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Language</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <span class="divider">/</span>
                        </li>
                        <li>
                            <a href="#">Language</a>
                            <span class="divider">/</span>
                        </li>

                        <li>
                            <span><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Language</span>

                        </li>





                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Edit Language</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <?php $categoryRowset = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM `webshop_language` WHERE `id`='" . $_REQUEST['id'] . "'")); ?>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post">
                                <div id="tbod">

                                    

                                    <div class="input_fields_wrap">
                                        <?php
                                
                                            ?>

                                            <div class="control-group" style="width:40%;float:left;">                                                
                                                <label class="control-label">Actual Word </label>
                                                <div class="controls">
                                                    <input type="text" class="form-control" placeholder="Language" value="<?php echo $categoryRowset['actual_word']; ?>" name="actual_word" required readonly>
                                                     <input type="hidden" class="form-control" placeholder="Language" value="<?php echo $categoryRowset['id']; ?>" name="id" required readonly>
                                                     <input type="hidden" class="form-control" placeholder="Language" value="<?php echo $_REQUEST['type']; ?>" name="type" required readonly>

                                        
                                                   
                                                </div>
                                            </div>
                                            
                                        <div class="control-group" style="width:40%;float:left;">                                                
                                                <label class="control-label">Transleted Word</label>
                                                <div class="controls">
                                                    <input type="text" class="form-control" placeholder="Transleted Word" value="<?php echo $categoryRowset['transleted_word']; ?>" name="transleted_word" required>

                                                </div>
                                            </div>
                                            
                                            <div class='clearfix'>&nbsp; </div>


                                            
                                       
                                        <!--<button class="add_field_button">Add More Fields</button>-->
                                    </div>

                                </div>




                        </div>
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                        <button type="reset" class="btn"><i class=" icon-remove"></i> Cancel</button>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">

        </div>
    </div>

    <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- Footer Start -->

<?php include("includes/footer.php"); ?>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/mdtimepicker.css">
<!-- Footer End -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="js/jquery-1.8.3.min.js"></script>
<!--<script src="js/jquery.nicescroll.js" type="text/javascript"></script>-->
<script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<script src="assets/bootstrap/js/bootstrap.min.js"></script>



<script src="js/jquery.sparkline.js" type="text/javascript"></script>

<script src="js/jquery.scrollTo.min.js"></script>


<!--common script for all pages-->
<script src="js/common-scripts.js"></script>




<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
