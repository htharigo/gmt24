<?php
include_once("./includes/config.php");
include_once('includes/session.php');
?>

<?php
if (isset($_REQUEST['submit'])) {

   
    $code = isset($_POST['code']) ? $_POST['code'] : '';
    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $currency_rate_to_usd = isset($_POST['currency_rate_to_usd']) ? $_POST['currency_rate_to_usd'] : '';
    //print_r($code);
    for ($w = 0; $w < count($code); $w++) {
        $code1 = $code[$w];
        $name1 =  $name[$w];
        $currency_rate_to_usd1 = $currency_rate_to_usd[$w];
        $fields = array(
            
            'code' => mysqli_real_escape_string($con, $code1),
            'name' => mysqli_real_escape_string($con, $name1),
            
        );

         $fields1 = array(
            
            'currency_code' => mysqli_real_escape_string($con, $code1),
            'currency_rate_to_usd' => mysqli_real_escape_string($con, $currency_rate_to_usd1),
            
        );
         
        $fieldsList = array();
        foreach ($fields as $field => $value) {
            $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
        }
        
         $fieldsList1 = array();
        foreach ($fields1 as $field1 => $value1) {
            $fieldsList1[] = '`' . $field1 . '`' . '=' . "'" . $value1 . "'";
        }
        
        $insertQuery = "INSERT INTO `webshop_currency` (`" . implode('`,`', array_keys($fields)) . "`)"
                . " VALUES ('" . implode("','", array_values($fields)) . "')";
        $insertQuery1 = "INSERT INTO `webshop_currency_rates` (`" . implode('`,`', array_keys($fields1)) . "`)"
                . " VALUES ('" . implode("','", array_values($fields)) . "')";
        
        

        mysqli_query($con, $insertQuery);
        $last_id = mysqli_insert_id($con);
        
        if (!empty($_FILES['imagee'])) {
                   
            
                               if ($_FILES['imagee']['tmp_name'] != '') {
                                       // print_r($_FILES);
                                       // $target_path = "http://thegmt24.com/app/assets/images/";
                                         $target_path = "../app/assets/images/";
                                        //echo $_FILES['name'];
                                        $userfile_name = $_FILES['imagee']['name'] [0] ;

                                        $userfile_tmp = $_FILES['imagee']['tmp_name'] [0] ;


                                        $img = $target_path . $userfile_name;
                                        if(move_uploaded_file($userfile_tmp, $img)){
                                     
                                        $image = mysqli_query($con, "UPDATE  `webshop_currency` set `image`='".$userfile_name."' WHERE `id`='".$last_id."'");
                                     
                                        }
                                    }
                               
                   
//print_r($_FILES['image']);exit;
                                
                            }
    }
    header('Location:list_currency.php');
    exit();
}
?>

<!-- Header Start -->
<?php include ("includes/header.php"); ?>


<!-- Header End -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Currency <small><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Currency</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <span class="divider">/</span>
                        </li>
                        <li>
                            <a href="#">Currencys</a>
                            <span class="divider">/</span>
                        </li>

                        <li>
                            <span><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Currency</span>

                        </li>





                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Add Currency</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div id="tbod">

                                    
                                    <div class="input_fields_wrap">
                                        <div class="clearfix">
                                            
                                         <div class="control-group" style="width:50%;float:left;">
                                            <label class="control-label">Currency Name</label>
                                            <div class="controls">
                                                <input type="text" id="time_2" class="form-control timepicker" placeholder="Currency Name" value="<?php echo $categoryRowset['name']; ?>" name="name[]" required>
                                            </div>
                                        </div>
                                            
                                        <div class="control-group" style="width:50%;float:left; max-width:330px;">
                                            <label class="control-label">Currency Code </label>
                                            <div class="controls">
                                                <input type="text" class="form-control timepicker"  placeholder="Currency" value="<?php echo $categoryRowset['code']; ?>" name="code[]" required>
                                            </div>
                                        </div>


                                      
                                            
                                            <div class="control-group" style="width:50%;float:left;">
                                            <label class="control-label">Currency Rate</label>
                                            <div class="controls">
                                                <input type="text" class="form-control" placeholder="Currency Rate" value="<?php echo $categoryRowset['currency_rate_to_usd']; ?>" name="currency_rate_to_usd[]" required>
                                            </div>
                                        </div>
                                            
                                            <div class="control-group">
                                    <label class="control-label">Upload Currency Image</label>
<!--                                    <div class="controls">
                                        <input type="file" name="imagee" class=" btn blue"  ><?php if ($categoryRowset['image'] != '') { ?><br><a href="../upload/product_image/<?php echo $categoryRowset['image']; ?>" target="_blank">View</a><?php } ?>
                                    </div>-->
                                        <div class="controls">
                                        <input type="file" name="imagee[]" multiple="multiple"  class=" btn blue"  >
                                    </div>
                                </div>
                                        </div>
                                        <!--<button class="add_field_button">Add More Fields</button>-->
                                    </div>
                                </div>




                        


                    <div class="form-actions">
                        <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                        <button type="reset" class="btn"><i class=" icon-remove"></i> Reset</button>
                    </div>
                    </form>
                            <a href="list_currency.php"><button class="btn"> Back</button></a>
                    <!-- END FORM-->
                </div>
                </div>
                    </div>
                
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">

        </div>
    </div>

    <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- Footer Start -->

<?php include("includes/footer.php"); ?>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/mdtimepicker.css">
<!-- Footer End -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="js/jquery-1.8.3.min.js"></script>
<!--<script src="js/jquery.nicescroll.js" type="text/javascript"></script>-->
<!--<script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->

<script src="assets/bootstrap/js/bootstrap.min.js"></script>


<!-- ie8 fixes -->


<script src="js/jquery.sparkline.js" type="text/javascript"></script>

<script src="js/jquery.scrollTo.min.js"></script>


<!--common script for all pages-->
<script src="js/common-scripts.js"></script>

<!--script for this page only-->




<script>


    $(document).ready(function () {
        var i = 3;
        var field = '';
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        var field = '   <div class="time"><div class="control-group" style="width:50%;float:left; max-width:330px;">  ' +
                '                                           <label class="control-label">Currency </label>  ' +
                '                                           <div class="controls">  ' +
                '                                               <input type="text" class="form-control timepicker" placeholder="Currency" value="<?php echo $categoryRowset["code"]; ?>" name="code[]" required>  ' +
                '                                           </div>  ' +
                '                                       </div>  ' +
                '     ' +
                '     ' +
                '                                       <div class="control-group" style="width:50%;float:left;">  ' +
                '                                           <label class="control-label">Currency Name</label>  ' +
                '                                           <div class="controls">  ' +
                '                                               <input type="text" class="form-control timepicker" placeholder="Currency Name" value="<?php echo $categoryRowset["name"]; ?>" name="name[]" required>  ' +
                '</div> </div> ' + 
                '<div class="control-group" style="width:50%;float:left;"> ' + 
                '<label class="control-label">Currency Rate </label> ' +'<div class="controls"> ' +
                '<input type="text" class="form-control timepicker" placeholder="Currency" value="<?php echo $categoryRowset["currency_rate_to_usd"]; ?>" name="currency_rate_to_usd[]" required>  '+
                '</div>  '+ '</div>  <a href="#" class="remove_field">Remove</a>'
                
        
    ;

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment

                $(wrapper).append(field); //add input box

               
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });
  

</script>






<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>