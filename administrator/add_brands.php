<?php 
include_once("./includes/session.php");
//include_once("includes/config.php");
include_once("./includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');

//thumb image code

function cwUpload($field_name = '', $target_folder = '', $file_name = '', $thumb = FALSE, $thumb_folder = '', $thumb_width = '', $thumb_height = ''){

    //folder path setup
    $target_path = $target_folder;
    $thumb_path = $thumb_folder;
    
    //file name setup
    $filename_err = explode(".",$_FILES[$field_name]['name']);
    $filename_err_count = count($filename_err);
    $file_ext = $filename_err[$filename_err_count-1];
    if($file_name != ''){
        $fileName = $file_name.'.'.$file_ext;
    }else{
        $fileName = $_FILES[$field_name]['name'];
    }
    
    //upload image path
    $upload_image = $target_path.basename($fileName);
    
    //upload image
    if(move_uploaded_file($_FILES[$field_name]['tmp_name'],$upload_image))
    {
        //thumbnail creation
        if($thumb == TRUE)
        {
            $thumbnail = $thumb_path.$fileName;
            list($width,$height) = getimagesize($upload_image);
            $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
            //$black = imagecolorallocate($thumb_create1, 0, 0, 0);
            //$thumb_create= imagecolortransparent($thumb_create1, $black);
           // $thumb_create = '';
            switch($file_ext){
                case 'jpg':
                    $source = imagecreatefromjpeg($upload_image);
                    break;
                case 'jpeg':
                    $source = imagecreatefromjpeg($upload_image);
                    break;

                case 'png':
                    $source = imagecreatefrompng($upload_image);
                    break;
                case 'gif':
                    $source = imagecreatefromgif($upload_image);
                    break;
                default:
                    $source = imagecreatefromjpeg($upload_image);
            }

            imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
            switch($file_ext){
                case 'jpg' || 'jpeg':
                    imagejpeg($thumb_create,$thumbnail,100);
                    break;
                case 'png':
                    imagepng($thumb_create,$thumbnail,100);
                    break;

                case 'gif':
                    imagegif($thumb_create,$thumbnail,100);
                    break;
                default:
                    imagejpeg($thumb_create,$thumbnail,100);
            }

        }

        return $fileName;
    }
    else
    {
        return false;
    }
}
//end thumb image code

     function makethumbnail($file_path,$file_name,$new_width, $new_height, $orginal="")
        {
            $original_image_path=$file_path.$file_name; // the path with the file name where the file will be stored, upload is the directory name.
            //$file_path=$original_image_path;
            $file_path=$file_path."thumb/thumb_".$file_name;
            $ImageArray = maintainAspectRatio($new_width, $original_image_path);
            $new_width=$ImageArray['width'];
            $new_height=$ImageArray['height'];
            
            $len = strlen($file_name); 
            $pos =strpos($file_name,"."); 
            $type = substr($file_name,$pos + 1,$len); 
            $file_type = strtolower($type);
        
            ///////// Start the thumbnail generation//////////////
            if (!($file_type =="jpeg" OR $file_type=="jpg" OR $file_type=="gif" OR $file_type=="png"))
            {
                echo "Your uploaded file must be of JPG or GIF. Other file types are not allowed<BR>";
                exit;
            }
            /////////////////////////////////////////////// Starting of GIF thumb nail creation///////////
            if (@$file_type=="gif")
            {
                $im=ImageCreateFromGIF($original_image_path);
                $width=ImageSx($im);              // Original picture width is stored
                $height=ImageSy($im);                  // Original picture height is stored
                $newimage=imagecreatetruecolor($new_width,$new_height);
                imageCopyResized($newimage,$im,0,0,0,0,$new_width,$new_height,$width,$height);
                $exif = exif_read_data($original_image_path);
              
                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        case 3:
                            $newimage = imagerotate($newimage, -180, 0);
                            break;
                        case 6:
                            $newimage = imagerotate($newimage, -90, 0);
                            break;
                        case 8:
                            $newimage = imagerotate($newimage, -90, 0);
                            break;
                    } 
                }
                if (function_exists("imagegif")) 
                {
                    header("Content-type: image/gif");
                    ImageGIF($newimage,$file_path);
                }
                elseif (function_exists("imagejpeg")) 
                {
                    header("Content-type: image/jpeg");
                    ImageJPEG($newimage,$file_path);
                }
                chmod("$file_path",0777);
            }////////// end of gif file thumb nail creation//////////
            
            ////////////// starting of JPG thumb nail creation//////////
            if($file_type=="jpeg" || $file_type=="jpg")
            {
                $im=ImageCreateFromJPEG($original_image_path); 
                $width=ImageSx($im);              // Original picture width is stored
                $height=ImageSy($im);             // Original picture height is stored
                $newimage=imagecreatetruecolor($new_width,$new_height);                 
                imageCopyResized($newimage,$im,0,0,0,0,$new_width,$new_height,$width,$height);
                $exif = exif_read_data($original_image_path);
              
                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        case 3:
                            $newimage = imagerotate($newimage, -180, 0);
                            break;
                        case 6:
                            $newimage = imagerotate($newimage, -90, 0);
                            break;
                        case 8:
                            $newimage = imagerotate($newimage, -90, 0);
                            break;
                    } 
                }
                ImageJpeg($newimage,$file_path);
                chmod("$file_path",0777);
            }
            if($file_type=="png")
            {
                $im=imagecreatefrompng($original_image_path); 
                $width=ImageSx($im);              // Original picture width is stored
                $height=ImageSy($im);             // Original picture height is stored
                $newimage=imagecreatetruecolor($new_width,$new_height);                 
                imageCopyResized($newimage,$im,0,0,0,0,$new_width,$new_height,$width,$height);
                $exif = exif_read_data($original_image_path);
              
                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        case 3:
                            $newimage = imagerotate($newimage, -180, 0);
                            break;
                        case 6:
                            $newimage = imagerotate($newimage, -90, 0);
                            break;
                        case 8:
                            $newimage = imagerotate($newimage, -90, 0);
                            break;
                    } 
                }
                imagepng($newimage,$file_path);
                chmod("$file_path",0777);
            }
            ////////////////  End of JPG thumb nail creation //////////
        }

         function maintainAspectRatio($FLASH_WIDTH,$image_url)
        {
            $arrImageSize = @getimagesize(trim($image_url),$arrImageSize);
            $width = $arrImageSize[0];
            $height = $arrImageSize[1];
            if($width > $FLASH_WIDTH )
                {
                $target_width = $FLASH_WIDTH;
                $target_height = round($height * ($FLASH_WIDTH/$width));
                if($target_height > 350)
                    {
                    $target_width = round($FLASH_WIDTH * (350/$target_height));
                    $target_height = 350;
                    }
                    
                }
            else
                {
                $target_width = $width;
                if($height > 350)
                    {
                    $target_height = 350;
                    $target_width = round($width * (350/$height));
                    }
                    else
                    {
                    $target_height = $height;
                    }
                }
            return array("width"=>$target_width,"height"=>$target_height,"image_url"=>$image_url);
        }



?>
<?php
//echo $_SESSION['myy'];

CRYPT_BLOWFISH or die ('No Blowfish found.');




if((!isset($_REQUEST['submit'])) && (!isset($_REQUEST['action'])))
{

 $sql="select * from webshop_brands  where id<>''";
 

$record=mysqli_query($con,$sql);


}

if(isset($_REQUEST['submit']))
{
    
 
  $name = isset($_POST['name']) ? $_POST['name'] : '';
 $Is_important = isset($_POST['Is_important']) ? $_POST['Is_important'] : '';
	

	$fields = array(
    'name' => mysqli_real_escape_string($con,$name),
            'Is_important' => mysqli_real_escape_string($con,$Is_important),
		);

		$fieldsList = array();
		foreach ($fields as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
					 
	 if($_REQUEST['action']=='edit')
	  {		  
	  $editQuery = "UPDATE `webshop_brands` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysqli_real_escape_string($con,$_REQUEST['id']) . "'";

    

      mysqli_query($con,$editQuery_user);

		if (mysqli_query($con,$editQuery)) {
		
		if($_FILES['image']['tmp_name']!='')
		{
                    
                    
                //$upload_img = cwUpload('image','../upload/','',TRUE,'../upload/thumbs/','200','181');
    
                 //full path of the thumbnail image
                //$thumb_src = '../upload/thumbs/'.$upload_img;   
                    
                    
		$target_path="../upload/brand_image/";
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$img_name =$userfile_name;
		$img=$target_path.$img_name;
		move_uploaded_file($userfile_tmp, $img);
                
        makethumbnail($target_path,$img_name,'146', '', $orginal="");
		
		$image =mysqli_query($con,"UPDATE `webshop_brands` SET `image`='".$img_name."' WHERE `id` = '" . mysqli_real_escape_string($con,$_REQUEST['id']) . "'");
		}
		
		
			$_SESSION['msg'] = "Tool Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occurred while updating Tool";
		}

		header('Location:list_brands.php');
		exit();
	
	 }





	 else
	 {
	 
 $addQuery = "INSERT INTO `webshop_brands` (`" . implode('`,`', array_keys($fields)) . "`)"
			. " VALUES ('" . implode("','", array_values($fields)) . "')";

  mysqli_query($con,$addQuery);
   $last_id=mysqli_insert_id($con);

	
		if($_FILES['image']['tmp_name']!='')
		{
		$target_path="../upload/brand_image/";
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$img_name =$userfile_name;
		$img=$target_path.$img_name;
		move_uploaded_file($userfile_tmp, $img);
		
		$image =mysqli_query($con,"UPDATE `webshop_brands` SET `image`='".$img_name."' WHERE `id` = '" . $last_id . "'");
		}
		 
		header('Location:list_brands.php');
		exit();
	
	 }
				
				
}

if($_REQUEST['action']=='edit')
{
$categoryRowset = mysqli_fetch_array(mysqli_query($con,"SELECT * FROM `webshop_brands` WHERE `id`='".mysqli_real_escape_string($con,$_REQUEST['id'])."'"));
}

    
?>




 <!-- Header Start -->
<?php include ("includes/header.php"); ?>
<!-- Header End -->
 <!-- BEGIN CONTAINER -->
   <div id="container" class="row-fluid">
      <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN THEME CUSTOMIZER-->
                   <div id="theme-change" class="hidden-phone">
                       <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                   </div>
                   <!-- END THEME CUSTOMIZER-->
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title">
                   Brand <small><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Brand</small>
                   </h3>
                   <ul class="breadcrumb">
                       <li>
                           <a href="#">Home</a>
                           <span class="divider">/</span>
                       </li>
                       <li>
                           <a href="#">Brand</a>
                           <span class="divider">/</span>
                       </li>
                       
                       <li>
                          <span><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Brand</span>
                          
                       </li>
                       
                       

                       
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Add Brand</h4>
                              <!-- <a style="float:right;" class="btn blue" href="sampledownload.csv">Download Sample</a> -->
                            <!--   <a style="float:right;" class="btn blue" href="upload_csv.php">Upload Csv</a> -->
                            <span class="tools">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post" action="add_brands.php" enctype="multipart/form-data">

                          <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                          <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />  
                           
                           <div class="control-group">
                                    <label class="control-label">Brand Name</label>
                                    <div class="controls">
                                    <input type="text" class="form-control" placeholder="Enter text" value="<?php echo $categoryRowset['name'];?>" name="name" required>
                                    </div>
                                </div>
                          
                          <div class="control-group">
                                    <label class="control-label">Set Priority</label>
                                    <div class="controls">
                                    <input type="number" class="form-control" placeholder="Enter priority number" value="<?php echo $categoryRowset['Is_important'];?>" name="Is_important" required>
                                    </div>
                                </div>

                                 <div class="control-group">
                                    <label class="control-label">Image Upload</label>
                                    <div class="controls">
                                        <input type="file" name="image" class=" btn blue"  ><?php if($categoryRowset['image']!=''){?><br><a href="../upload/brand_image/<?php echo $categoryRowset['image'];?>" target="_blank">View</a><?php }?>
                                        
                                    </div>
                                </div>
                              
                                <div class="form-actions">
                                    <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                                    <button type="reset" class="btn"><i class=" icon-remove"></i> Reset</button>
                                </div>
                            </form>
                            <a href="list_brands.php"><button class="btn"> Back</button></a>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                   
                </div>
            </div>

            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->
   </div>
   <!-- END CONTAINER -->

   <!-- Footer Start -->

   <?php include("includes/footer.php"); ?>

   <!-- Footer End -->
    <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="js/jquery-1.8.3.min.js"></script>
<!--   <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->
   <script src="assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="assets/chart-master/Chart.js"></script>
   <script src="js/jquery.scrollTo.min.js"></script>


   <!--common script for all pages-->
   <script src="js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="js/easy-pie-chart.js"></script>
   <script src="js/sparkline-chart.js"></script>
   <script src="js/home-page-calender.js"></script>
   <script src="js/home-chartjs.js"></script>
    <script src="assets/ckeditor/ckeditor.js" type="text/javascript"></script>

   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>
