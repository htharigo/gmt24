<?php
include_once("./includes/config.php");
include_once('includes/session.php');
?>

<?php
if (isset($_REQUEST['submit'])) {

    $date = date('Y-m-d');
    $movement = isset($_POST['movement']) ? $_POST['movement'] : '';
    

    //print_r($movement);
    for ($w = 0; $w < count($movement); $w++) {
        $movement1 =  $movement[$w];
        

        $fields = array(
            'date' => mysqli_real_escape_string($con, $date),
            'name' => mysqli_real_escape_string($con, $movement1),
            'status' => 1,
           
        );
        //print_r($fields);
        
        $fieldsList = array();
        foreach ($fields as $field => $value) {
            $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
        }

        $insertQuery = "INSERT INTO `webshop_movement` (`" . implode('`,`', array_keys($fields)) . "`)"
                . " VALUES ('" . implode("','", array_values($fields)) . "')";

        mysqli_query($con, $insertQuery);
        $last_id = mysqli_insert_id($con);
    }
    header('Location:list_movement.php');
    exit();
}
?>

<!-- Header Start -->
<?php include ("includes/header.php"); ?>


<!-- Header End -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Add Movement <small><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Movement</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <span class="divider">/</span>
                        </li>
                        <li>
                            <a href="#">Movement</a>
                            <span class="divider">/</span>
                        </li>

                        <li>
                            <span><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Movement</span>

                        </li>





                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Add Movement</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post">
                                <div id="tbod">

                                    
                                    <div class="input_fields_wrap">
                                        <div class="clearfix">
                                        <div class="control-group" style="width:50%;float:left; max-width:330px;">
                                            <label class="control-label">Movement Name : </label>
                                            <div class="controls">
                                                <input type="text" class="form-control timepicker"  placeholder="Movement Name" value="<?php echo $categoryRowset['movement']; ?>" name="movement[]" required>
                                            </div>
                                        </div>

                                        </div>
                                        <!--<button class="add_field_button">Add More Fields</button>-->
                                    </div>
                                </div>




                        


                    <div class="form-actions">
                        <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                        <button type="reset" class="btn"><i class=" icon-remove"></i> Reset</button>
                    </div>
                    </form>
                            <a href="list_movement.php"><button class="btn"> Back</button></a>
                    <!-- END FORM-->
                </div>
                </div>
                    </div>
                
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">

        </div>
    </div>

    <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- Footer Start -->

<?php include("includes/footer.php"); ?>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/mdtimepicker.css">
<!-- Footer End -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="js/jquery-1.8.3.min.js"></script>
<!--<script src="js/jquery.nicescroll.js" type="text/javascript"></script>-->
<!--<script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->

<script src="assets/bootstrap/js/bootstrap.min.js"></script>


<!-- ie8 fixes -->

<script src="js/jquery.scrollTo.min.js"></script>


<!--common script for all pages-->
<script src="js/common-scripts.js"></script>







<script>


    $(document).ready(function () {
        var i = 3;
        var field = '';
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        var field = '   <div class="time"><div class="control-group" style="width:50%;float:left; max-width:330px;">  ' +
                '                                           <label class="control-label">Start Time </label>  ' +
                '                                           <div class="controls">  ' +
                '                                               <input type="text" class="form-control timepicker" placeholder="Start Time" value="<?php echo $categoryRowset["movement"]; ?>" name="movement[]" required>  ' +
                '                                           </div>  ' +
                '                                       </div> <a href="#" class="remove_field">Remove</a> ';

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment

                $(wrapper).append(field); //add input box

            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });
  

</script>






<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>