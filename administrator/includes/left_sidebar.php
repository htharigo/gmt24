<?php
$pagename = end(explode('/', $_SERVER['REQUEST_URI']));
$prevnameall = explode(',', $_SESSION['privilege_name']);
//print_r($pagename);
//print_r($prevnameall);
?>
<!-- BEGIN SIDEBAR -->


<style>
    #sidebar > ul > li > ul.sub > li > a{
        font-size: 10px !important;
        padding: 10px 10px 10px 30px !important;
    }

    .green .widget-title span.tools{
        display: none;
    }
</style>


<div class="sidebar-scroll">
    <div id="sidebar" class="nav-collapse collapse">

        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <div class="navbar-inverse">
            <form class="navbar-search visible-phone">
                <input type="text" class="search-query" placeholder="Search" />
            </form>
        </div>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
        <!-- BEGIN SIDEBAR MENU -->





        <ul class="sidebar-menu">              

            <li class="sub-menu active">
                <a class="" href="dashboard.php">
                    <i class="icon-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            
             <?php if (in_array('3', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-user"></i>
                        <span>Users <br> Management</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">

                                                                                                                                                                           </li>
                        <li  <?php if ($pagename == 'list_user.php') { ?>  class="active" <?php } ?>>
                            <a href="list_user.php">
                                <i class="icon-bulb"></i>
                                Normal User</a>
                        </li> 

                        <li  <?php if ($pagename == 'list_topuser.php') { ?>  class="active" <?php } ?>>
                            <a href="list_topuser.php">
                                <i class="icon-bulb"></i>
                                Certified User</a>
                        </li>
                        <li  <?php if ($pagename == 'list_blocked_vendor.php') { ?>  class="active" <?php } ?>>
                            <a href="list_blocked_user.php">
                                <i class="icon-bulb"></i>
                                Blocked User </a>
                        </li> 
                        <li  <?php if ($pagename == 'list_vendor.php') { ?>  class="active" <?php } ?>>
                            <a href="list_vendor.php">
                                <i class="icon-bulb"></i>
                                Normal Vendor</a>
                        </li> 

                        <li  <?php if ($pagename == 'list_topvendor.php') { ?>  class="active" <?php } ?>>
                            <a href="list_topvendor.php">
                                <i class="icon-bulb"></i>
                                Certified Vendor</a>
                        </li>
                        
                        <li  <?php if ($pagename == 'list_blocked_vendor.php') { ?>  class="active" <?php } ?>>
                            <a href="list_blocked_vendor.php">
                                <i class="icon-bulb"></i>
                                Blocked Vendor </a>
                        </li>
                        
                        <li  <?php if ($pagename == 'add_loyalietypoint.php') { ?>  class="active" <?php } ?>>
                            <a href="add_loyalietypoint.php">
                                <i class="icon-bulb"></i>
                                Add Loyalty</a>
                        </li> 

                        <li  <?php if ($pagename == 'list_loyalietypoint.php') { ?>  class="active" <?php } ?>>
                            <a href="list_loyalietypoint.php">
                                <i class="icon-bulb"></i>
                                list Loyalty</a>
                        </li>
                        <li  <?php if ($pagename == 'list_loyaliety_user.php') { ?>  class="active" <?php } ?>>
                            <a href="list_loyaliety_user.php">
                                <i class="icon-bulb"></i>
                                Loyalty user Point</a>
                        </li>
                        
                        <li>
                        <a href="add_subscription.php">
                             <i class="icon-bulb"></i>
                            Add Subscription</a>
                    </li>

                    <li>
                        <a href="list_subscription.php">
                             <i class="icon-bulb"></i>
                            List Subscription</a>
                    </li>
                    </ul>
                </li>
                <?php
            }
            ?>
              <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon-usd"></i>
                    <span>Product <br>Management</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub"> 

                    <li>
                        <a href="list_product.php">
                            <i class="icon-bulb"></i>
                            Request Product</a>
                    </li> 
                    <li>
                        <a href="list_live_product.php">
                            <i class="icon-bulb"></i>
                            List Live Product</a>
                    </li>
                    
                    <li>
                        <a href="list_all_products.php">
                            <i class="icon-bulb"></i>
                            All Product</a>
                    </li> 
                    <li>
                        <a href="add_user_subscription.php">
                             <i class="icon-bulb"></i>
                            Add Product Packages</a>
                    </li>

                    <li>
                        <a href="list_user_subscription.php">
                             <i class="icon-bulb"></i>
                            List Product Packages</a>
                    </li>
                    
                   
                    <li>
                        <a href="add_top_subscription.php">
                             <i class="icon-bulb"></i>
                            Add Top Product Packages</a>
                    </li>

                    <li>
                        <a href="list_top_subscription.php">
                             <i class="icon-bulb"></i>
                            List Top Product Packages</a>
                    </li>
                    <li>
                            <a href="freebid_settings.php">
                                <i class="icon-bulb"></i>
                                Free Product Setting</a>
                        </li>
                </ul>
            </li>
            
            <?php if (in_array('15', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-usd"></i>
                        <span>Auction <br>Management</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub"> 
                        <li>
                            <a href="manage_percentage.php">
                                <i class="icon-bulb"></i>
                                Manage Percentage</a>
                        </li>
                        <li  <?php if ($pagename == 'list_auction.php') { ?>  class="active" <?php } ?>>
                            <a href="list_auction.php">
                                <i class="icon-bulb"></i>
                                Request Auction</a>
                        </li> 
                        <li  <?php if ($pagename == 'list_live_auction.php') { ?>  class="active" <?php } ?>>
                            <a href="list_live_auction.php">
                                <i class="icon-bulb"></i>
                                List Live Auction</a>
                        </li> 
                        <li  <?php if ($pagename == 'list_expire_auction.php') { ?>  class="active" <?php } ?>>
                            <a href="list_expire_auction.php">
                                <i class="icon-bulb"></i>
                                List Expire Auction</a>
                        </li> 
                        
                         <li>
                        <a href="add_special_auction.php">
                            <i class="icon-bulb"></i>                            
                            Add Special Auction</a>
                    </li>
                    
                    <li>
                        <a href="add_auctiondates.php">
                            <i class="icon-bulb"></i>
                            Add Auction Date</a>
                    </li>

                    <li>
                        <a href="list_auctiondates.php">
                            <i class="icon-bulb"></i>
                            List Auction Date</a>
                    </li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon-tasks"></i>
                    <span>Manage Latest Deal</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">

                    <li>
                        <a href="add_leatest_deal.php">
                            Add Latest Deal</a>
                    </li>

                    

                </ul>
            </li>
            


            <?php if (in_array('22', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-archive"></i>
                        <span>Newsletter</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">


                        <li  <?php if ($pagename == 'newsletter_list.php') { ?>  class="active" <?php } ?>>
                            <a href="newsletter_list.php">
                                <i class="icon-archive"></i>
                                List Newsletter</a>
                        </li> 

                    </ul>
                </li>
                <?php
            }
            ?>

            

            <?php if (in_array('9', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-star"></i>
                        <span>Reviews <br> Management</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">


                        <li  <?php if ($pagename == 'list_all_reviews.php') { ?>  class="active" <?php } ?>>
                            <a href="list_all_reviews.php">
                                <i class="icon-bulb"></i>
                                List Reviews</a>
                        </li> 
                    </ul>
                </li>
                <?php
            }
            ?>


            <?php if (in_array('13', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-volume-up"></i>
                        <span>Advertisement</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">

               <li  <?php if ($pagename == 'add_advertisement.php') { ?>  class="active" <?php } ?>>
                <a href="add_advertisement.php?action=submit">
                  <i class="icon-bulb"></i>
                   Add Advertisement</a>
            </li>  

                        <li  <?php if ($pagename == 'list_advertisement.php') { ?>  class="active" <?php } ?>>
                            <a href="list_advertisement.php">
                                <i class="icon-bulb"></i>
                                Manage Advertisement</a>
                        </li> 

                        <li  <?php if ($pagename == 'pending_advertisement.php') { ?>  class="active" <?php } ?>>
                            <a href="pending_advertisement.php">
                                <i class="icon-bulb"></i>
                                Pending Advertisement</a>
                        </li>

                        <li  <?php if ($pagename == 'paid_advertisement.php') { ?>  class="active" <?php } ?>>
                            <a href="paid_advertisement.php">
                                <i class="icon-bulb"></i>
                                Paid Advertisement</a>
                        </li> 
                    </ul>
                </li>
                <?php
            }
            ?>
              
            <?php if (in_array('12', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-tag"></i>
                        <span> Messages</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li  <?php if ($pagename == 'message_user.php') { ?>  class="active" <?php } ?>>
                            <a href="message_user.php">
                                <i class="icon-bulb"></i>
                                Message User</a>
                        </li>

                        <li  <?php if ($pagename == 'message_vendor.php') { ?>  class="active" <?php } ?>>
                            <a href="message_vendor.php">
                                <i class="icon-bulb"></i>
                                Message Vendor</a>
                        </li> 
                        
                        <li  <?php if ($pagename == 'selective_customer.php') { ?>  class="active" <?php } ?>>
                            <a href="selective_customer.php">
                                <i class="icon-bulb"></i>
                                Broadcast Message To Selective Customer</a>
                        </li> 

                        <li  <?php if ($pagename == 'selective_vendor.php') { ?>  class="active" <?php } ?>>
                            <a href="selective_vendor.php">
                                <i class="icon-bulb"></i>
                                Broadcast Message To Selective Vendor</a>
                        </li> 
                    </ul>
                </li>
                <?php
            }
            ?>  

            <?php if (in_array('11', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-bell-alt"></i>
                        <span>Notification Setting</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">

                        <li>
                            <a href="notification_list.php">
                                <i class="icon-bulb"></i>
                                Notification List</a>
                        </li>


                        <li>
                            <a href="notification_settings.php">
                                <i class="icon-bulb"></i>
                                Notification Settings</a>
                        </li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <?php if (in_array('1', $prevnameall)) { ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-book"></i>
                        <span>Site Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
            
                        <li>
                            <a href="add_social.php">
                                <i class="icon-bulb"></i>
                                Add Social Media</a>
                        </li>

                        <li>

                            <a href="social.php">
                                <i class="icon-bulb"></i>
                                Manage Social Media</a>
                        </li>

                        <li>
                            <a href="list_banner.php">
                                <i class="icon-bulb"></i>
                                Banner Management </a>
                        </li>

                        <li>
                            <a href="contact_setting.php">
                                <i class="icon-bulb"></i>
                                Manage Contact Info</a>
                        </li>
                        
                        <li>
                            <a href="manage_footer_text.php">
                                <i class="icon-bulb"></i>
                                Manage Footer Text</a>
                        </li>
                        
                        <li>
                            <a href="manage_advertisement.php">
                                <i class="icon-bulb"></i>
                                Advertisement Type</a>
                        </li>
                        
                        <li>
                        <a href="language.php">
                            <i class="icon-bulb"></i>
                            Add Words</a>
                    </li>
                    <li>
                        <a href="list_english.php">
                            <i class="icon-bulb"></i>
                            List English Words</a>
                    </li>
                    <li>
                        <a href="list_arabic.php">
                            <i class="icon-bulb"></i>
                            List Arabic Words</a>
                    </li>
                    <li>
                            <a href="email_template_list.php">
                                <i class="icon-bulb"></i>
                                Email Template</a>
                        </li>
                        <li>
                            <a href="add_page_name.php">
                                <i class="icon-bulb"></i>
                                Manage CMS</a>
                        </li>
                        
                        <li>
                        <a href="add_topmodels.php">
                             <i class="icon-bulb"></i>
                            Add Top Models</a>
                    </li>

                    <li>
                        <a href="list_top_models.php">
                             <i class="icon-bulb"></i>
                            List Top Models</a>
                    </li>

                    </ul>
                </li>

                <?php
            }
            ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-glass"></i>
                        <span>Other Management</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">


                       <li>
                        <a href="add_movement.php">
                            Add Movement</a>
                    </li>

                    <li>
                        <a href="list_movement.php">
                            List Movements</a>
                    </li>
                    <li>
                        <a href="add_watchstatus.php">
                            Add Watch Status</a>
                    </li>

                    <li>
                        <a href="list_watchstatus.php">
                            List Watch Status</a>
                    </li>
                    <li>
                        <a href="add_currencycode.php">
                            Add Currency code</a>
                    </li>

                    <li>
                        <a href="list_currency.php">
                            List Currency code</a>
                    </li>
                       <li  <?php if ($pagename == 'add_brands.php') { ?>  class="active" <?php } ?>>
                            <a href="add_brands.php">
                                <i class="icon-bulb"></i>
                                Add Brands</a>
                        </li> 

                        <li  <?php if ($pagename == 'list_brands.php') { ?>  class="active" <?php } ?>>
                            <a href="list_brands.php">
                                <i class="icon-bulb"></i>
                                List Brands</a>
                        </li> 
                        
                        <li  <?php if ($pagename == 'add_category.php') { ?>  class="active" <?php } ?>>
                            <a href="add_category.php">
                                <i class="icon-bulb"></i>
                                Add Sub Category</a>
                        </li> 

                        <li  <?php if ($pagename == 'list_category.php') { ?>  class="active" <?php } ?>>
                            <a href="list_category.php">
                                <i class="icon-bulb"></i>
                                List Sub Category</a>
                        </li> 
                    </ul>
                </li>
            <?php if (in_array('8', $prevnameall)) { ?>

                <li class="sub-menu">
                    <a href="javascript:;" class="">
                        <i class="icon-glass"></i>
                        <span>Profile Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">


                        <li>
                            <a href="changeusername.php">
                                <i class="icon-bulb"></i> Change Username </a>
                        </li>

                        <li>
                            <a href="changeemail.php">
                                <i class="icon-bulb"></i> Change Email </a>
                        </li>

                        <li>
                            <a href="changepassword.php">
                                <i class="icon-bulb"></i> Change Password </a>
                        </li>

                        <li>
                            <a href="logout.php">
                                <i class="icon-bulb"></i>
                                
                                Logout</a>
                        </li>
                    </ul>
                </li>

                <?php
            }
            ?>
                
                
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->