<?php
include_once("./includes/config.php");
include_once('includes/session.php');
?>

<?php
if (isset($_REQUEST['submit'])) {

   
    $status = isset($_POST['status']) ? $_POST['status'] : '';
    $rating = isset($_POST['rating']) ? $_POST['rating'] : '';

    //print_r($status);
    //$images ='';
        $status1 = $status;
        $rating1 =  $rating;
        if($rating1 == 1){
            $images = 'onestar.jpeg';
            $rating = '<i class="fa fa-star"></i>';
        }
        if($rating1 == 2){
            $images = 'twostar.jpeg';
            $rating = '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        if($rating1 == 3){
            $images = 'threestar.jpeg';
            $rating = '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        if($rating1 == 4){
            $images = 'fourstar.jpeg';
            $rating = '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        if($rating1 == 5){
            $images = 'fivestar.jpeg';
            $rating = '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
//echo $images;
        $fields = array(
            
            'status' => mysqli_real_escape_string($con, $status1),
            'rating' => mysqli_real_escape_string($con, $rating),
            'image' => mysqli_real_escape_string($con, $images),
            'rating_number'=> mysqli_real_escape_string($con, $rating1),
        );
//print_r($fields);
        $fieldsList = array();
        foreach ($fields as $field => $value) {
            $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
        }

       echo $insertQuery = "INSERT INTO `webshop_watchstatus` (`" . implode('`,`', array_keys($fields)) . "`)"
                . " VALUES ('" . implode("','", array_values($fields)) . "')";
//exit;
        mysqli_query($con, $insertQuery);
        $last_id = mysqli_insert_id($con);
   
    header('Location:list_watchstatus.php');
    exit();
}
?>

<!-- Header Start -->
<?php include ("includes/header.php"); ?>


<!-- Header End -->
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <?php include("includes/left_sidebar.php"); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme Color:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-green" data-style="green"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-red" data-style="red"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Watch Status <small><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Watch Status</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <span class="divider">/</span>
                        </li>
                        <li>
                            <a href="#">Watch Statuss</a>
                            <span class="divider">/</span>
                        </li>

                        <li>
                            <span><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Watch Statuss</span>

                        </li>





                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget green">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>Add Watch Status</h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                                <a href="javascript:;" class="icon-remove"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" method="post">
                                <div id="tbod">

                                    
                                    <div class="input_fields_wrap">
                                        <div class="clearfix">
                                        <div class="control-group" style="width:50%;float:left; max-width:330px;">
                                            <label class="control-label">Watch Status </label>
                                            <div class="controls">
                                                <input type="text" class="form-control timepicker"  placeholder="Watch Status" value="<?php echo $categoryRowset['status']; ?>" name="status" required>
                                            </div>
                                        </div>


                                        <div class="control-group" style="width:50%;float:left;">
                                            <label class="control-label">Rating</label>
                                            <div class="controls">
                                                <select name="rating" required>
                                                    <option value="">Select Rating</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    
                                                    
                                                </select>
                                                <!--<input type="text" id="time_2" class="form-control timepicker" placeholder="Rating" value="<?php echo $categoryRowset['rating']; ?>" name="rating[]" required>-->
                                            </div>
                                        </div>
                                        </div>
                                        <!--<button class="add_field_button">Add More Fields</button>-->
                                    </div>
                                </div>




                        


                    <div class="form-actions">
                        <button type="submit" class="btn blue" name="submit"><i class="icon-ok"></i> Save</button>
                        <button type="reset" class="btn"><i class=" icon-remove"></i> Reset</button>
                    </div>
                    </form>
                            <a href="list_watchstatus.php"><button class="btn"> Back</button></a>
                    <!-- END FORM-->
                </div>
                </div>
                    </div>
                
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">

        </div>
    </div>

    <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- Footer Start -->

<?php include("includes/footer.php"); ?>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/mdtimepicker.css">
<!-- Footer End -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->
<script src="js/jquery-1.8.3.min.js"></script>
<!--<script src="js/jquery.nicescroll.js" type="text/javascript"></script>-->
<!--<script type="text/javascript" src="assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>-->

<script src="assets/bootstrap/js/bootstrap.min.js"></script>


<!-- ie8 fixes -->


<script src="js/jquery.sparkline.js" type="text/javascript"></script>

<script src="js/jquery.scrollTo.min.js"></script>


<!--common script for all pages-->
<script src="js/common-scripts.js"></script>

<!--script for this page only-->




<script>


    $(document).ready(function () {
        var i = 3;
        var field = '';
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        var field = '   <div class="time"><div class="control-group" style="width:50%;float:left; max-width:330px;">  ' +
                '                                           <label class="control-label">Watch Status </label>  ' +
                '                                           <div class="controls">  ' +
                '                                               <input type="text" class="form-control timepicker" placeholder="Watch Status" value="<?php echo $categoryRowset["status"]; ?>" name="status[]" required>  ' +
                '                                           </div>  ' +
                '                                       </div>  ' +
                '     ' +
                '     ' +
                '                                       <div class="control-group" style="width:50%;float:left;">  ' +
                '                                           <label class="control-label">Rating</label>  ' +
                '                                           <div class="controls">  ' +
                '                                               <input type="text" class="form-control timepicker" placeholder="Rating" value="<?php echo $categoryRowset["rating"]; ?>" name="rating[]" required>  ' +
                '</div> </div><a href="#" class="remove_field">Remove</a> ';

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment

                $(wrapper).append(field); //add input box

               
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });
  

</script>






<!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>